# tiw8-tp1


# install project
$ git clone https://forge.univ-lyon1.fr/p2112327/tiw8-tp1.git

# open app directory
$ cd tiw8-tp1

# install dependencies
$ npm install

# serve with hot reload at localhost
$ npm run dev

# build for production
$ npm run build 

# launch  server at locahost
$ npm run start

# Link for heroku
https://aqueous-woodland-20920.herokuapp.com/


