import * as React from 'react'
// Import de l'image
import LOGO from '../image/logo.png'
import PACK from '../image/webpack.png'
import PIC from '../image/react.png'

// Utilisation

export default function Content () {
  return (<div >

          <img src={LOGO} alt="Logo" width="400" />
          <img src={PACK} alt="Web" width="400"/>
          <img src={PIC} alt="react" width="400"/>

          </div>
  )
}
